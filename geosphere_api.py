#!/bin/python3

import codecs
import json

import pandas as pd
import requests

pd.set_option("mode.copy_on_write", True)

FILE = "Stationsliste_20240101.csv"
URL = "https://dataset.api.hub.geosphere.at/v1/station/current/tawes-v1-10min"
COLUMN_HEADERS = {
    "SYNNR": "STATION_ID",
    "LÄNGE DEZI": "LON",
    "BREITE DEZI": "LAT",
}


def get_station_data(file) -> pd.DataFrame:
    with codecs.open(file, "r", encoding="ISO-8859-1") as f:
        data_frame_station = pd.read_csv(f, delimiter=";")

    data_frame_station = data_frame_station[data_frame_station["ORDNUNG"] != "Ö3"]
    data_frame_station = data_frame_station[
        ["SYNNR", "NAME", "LÄNGE DEZI", "BREITE DEZI"]
    ]
    data_frame_station.rename(columns=COLUMN_HEADERS, inplace=True)

    data_frame_station["LON"] = [
        float(str(i).replace(",", ".")) for i in data_frame_station["LON"]
    ]
    data_frame_station["LAT"] = [
        float(str(i).replace(",", ".")) for i in data_frame_station["LAT"]
    ]
    data_frame_station["STATION_ID"] = [
        int(i) for i in data_frame_station["STATION_ID"]
    ]

    return data_frame_station


def get_temperatures(data_frame_station: pd.DataFrame) -> pd.DataFrame:
    station_ids = data_frame_station["STATION_ID"]
    temperatures = []

    for station_id in station_ids:
        params = {"parameters": "TL", "station_ids": station_id}
        response = requests.get(url=URL, params=params)
        data = json.loads(response.text)

        try:
            temperature = data["features"][0]["properties"]["parameters"]["TL"]["data"][
                0
            ]
        except KeyError:
            temperature = None

        temperatures.append(temperature)

    data_frame_temperatures = pd.DataFrame(temperatures, columns=["TEMP"])

    return data_frame_temperatures


def write_temperatures_to_station_data_dataframe(
    data_frame_station: pd.DataFrame, data_frame_temperatures: pd.DataFrame
) -> pd.DataFrame:
    data_frame_for_dash = pd.concat(
        [data_frame_station, data_frame_temperatures], axis=1
    )

    return data_frame_for_dash


only_use_station_data_for_testing = get_station_data(file=FILE)
print(only_use_station_data_for_testing.head())
# def main():
#    station_data = get_station_data(file=FILE)
#    temperatures = get_temperatures(data_frame_station=station_data)
#    write_temperatures_to_station_data_dataframe(
#        data_frame_station=station_data, data_frame_temperatures=temperatures
#    )
#
#
# if __name__ == "__main__":
#    main()
