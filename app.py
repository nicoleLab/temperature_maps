#!/bin/python3

import plotly.express as px
from dash import Dash, dcc, html

from geosphere_api import only_use_station_data_for_testing

PORT = 8050
ADDRESS = "0.0.0.0"
CENTER_LAT = 47.6964
CENTER_LON = 13.3458

app = Dash(__name__)

aut_cities = only_use_station_data_for_testing

fig = px.scatter_mapbox(
    aut_cities,
    lat="LAT",
    lon="LON",
    hover_name="NAME",
    hover_data="TEMP",
    color_discrete_sequence=["fuchsia"],
    zoom=3,
    height=1000,
    width=2100,
)

fig.update_layout(mapbox_style="open-street-map")

fig.update_mapboxes(center_lat=CENTER_LAT, center_lon=CENTER_LON, zoom=6)

app.layout = html.Div(
    children=[
        html.H1(children="Hello Dash"),
        html.Div(
            children="""
        Dash: A web application framework for your data.
    """
        ),
        dcc.Graph(id="example-graph", figure=fig),
    ]
)

if __name__ == "__main__":
    app.run(debug=True, port=PORT, host=ADDRESS)
